FROM java:8
EXPOSE 8080
ADD /target/PracticeDocker.jar PracticeDocker.jar
ENTRYPOINT ["java","-jar","PracticeDocker.jar"]